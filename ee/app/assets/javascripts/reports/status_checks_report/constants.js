export const PASSED = 'passed';

export const APPROVED = 'approved';

export const PENDING = 'pending';

export const FAILED = 'failed';
